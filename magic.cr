require "socket"

macRegex = /^([0-9a-f]{2}[\.:-]){5}([0-9a-f]{2})$/
ipRegex = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/

if !ARGV.size == 2
  puts "usage: ./magic <MAC Address> <Broadcast Address>"
  exit 1
elsif !macRegex.match(ARGV[0])
  puts "The MAC address should be in this format: MM:MM:MM:SS:SS:SS"
  exit 1
elsif !ipRegex.match(ARGV[1]) || ARGV[1].split(".")[3] != "255"
  puts "The broadcast address should be in this format: [192-223].[0-255].[0-255].[255]"
  exit 1
end

mac = ARGV[0].gsub(":", "")
magicPacket = "FFFFFFFFFFFF" + mac*16
sock = Socket.udp(Socket::Family::INET)
sock.setsockopt(LibC::SO_BROADCAST,1)
ip = Socket::IPAddress.new(ARGV[1], 9)
sock.send(magicPacket.hexbytes, ip)

puts "\033[92m"+"WOL #{ARGV[0]} ---> #{ARGV[1]}"+"\033[0m"
